const express=require('express');

//Modelo=Usuario={email,nombre,apellido,pass}


function create(req,res,next){
    let email=req.body.email;
    let name=req.body.name;
    let lastName=req.body.lastName;
    let pass=req.body.pass;
    let user=new Object({
        email:email,
        name:name,
        lastName:lastName,
        pass:pass
    });
    //res.send(`Genera un nuevo usuario: ${user}`);
    res.json(user);
}
function list(req,res,next){
    let page=req.params.page ? req.params.page : 0;
    res.send(`Mostrar todos los usuarios de la pagina ${page}`);
}
function index(req,res,next){
    const id=req.params.id;
    res.send(`Mostrar el usuario con id=${id}`);
}
function edit(req,res,next){
    const id=req.params.id;
    res.send(`Modificar el usuario con id=${id}`);
}
function replace(req,res,next){
    const id=req.params.id;
    res.send(`Reemplazar el usuario con id=${id}`);
}
function remove(req,res,next){
    const id=req.params.id;
    res.send(`Elimina el usuario con id=${id}`);
}
module.exports={
    create, list, index, edit, replace, remove
}