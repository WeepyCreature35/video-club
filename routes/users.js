var express = require('express');
var router = express.Router();
const controller=require('../controllers/users')

router.post('/',controller.create);
router.get('/:page?',controller.list);
router.get('/show/:id',controller.index);
router.put('/:id',controller.edit);
router.patch('/:id',controller.replace);
router.delete('/:id',controller.remove);
//router.get('/registro/:name/:lastName/:email/:pass')
module.exports = router;
